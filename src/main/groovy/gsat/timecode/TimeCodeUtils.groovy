package gsat.timecode

class TimeCodeUtils {
	
	static final enum Units {MILLISECONDS, FRAME_NUMBER, COMPONENTS, STRING}
	
	def frameRate
	def dfSeparator = '.'
	
	boolean getDropFrame() {
		(frameRate > 29 && frameRate < 30) || (frameRate > 59 && frameRate < 60)
	}
	
	int getRoundFrameRate() {
		Math.round(frameRate)
	}
	
	long frameNumberToMilliseconds(int frameNumber) {
		Math.floor(frameNumber * 1000l /frameRate)
	}
	
	int[] frameNumberToComponents(int frameNumber) {
		def fn = frameNumber + calculateDropedFramesToAdd(frameNumber)
		int frames  =    fn % roundFrameRate
		int seconds =   (Math.floor(fn / roundFrameRate)) % 60
		int minutes =  (Math.floor(Math.floor(fn / roundFrameRate)) / 60) % 60
		int hours   = (Math.floor(Math.floor(Math.floor(fn / roundFrameRate)) / 60) / 60) % 24
		[hours, minutes, seconds, frames]
	}
	
	String frameNumberToString(int frameNumber) {
		format(frameNumberToComponents(frameNumber))
	}
	
	int millisecondsToFrameNumber(long milliseconds) {
		Math.ceil(Math.ceil(milliseconds * frameRate)/1000)
	}
	
	int[] millisecondsToComponents(long milliseconds) {
		frameNumberToComponents(millisecondsToFrameNumber(milliseconds))
	}
	
	String millisecondsToString(long milliseconds) {
		format(millisecondsToComponents(milliseconds))
	}
	
	long componentsToMilliseconds(components) {
		frameNumberToMilliseconds(componentsToFrameNumber(components))
	}

	long stringToMilliseconds(String string) {
		componentsToMilliseconds(parse(string))
	}

	int stringToFrameNumber(String string) {
		componentsToFrameNumber(parse(string))
	}
	
	int componentsToFrameNumber(components) {
		
		int hours = components[0]
		int minutes = components[1]
		int seconds = components[2]
		int frames = components[3]
		
		int totalMinutes = 60 * hours + minutes
		
		int frameNumber  = (3600*roundFrameRate) * hours + (60*roundFrameRate) * minutes + roundFrameRate * seconds + frames
		
		if (frameRate > 29.00 && frameRate < 30.00) {
			frameNumber -= 2 * (totalMinutes - (Math.floor(totalMinutes / 10)))
		}
		
		if (frameRate > 59.00 && frameRate < 60.00) {
			frameNumber -= 4 * (totalMinutes - (Math.floor(totalMinutes / 10)))
		}
		
		return frameNumber
	}
	
	int[] parse(string) {
		string.split('[:;.]')*.toInteger()
	}
	
	String d2(int i) {
		def s = '' + i
		if (i < 10)
			s = '0' + s
		s
	}

	
	String format(components, dfSep = null) {
		dfSep = dfSep ?: dfSeparator
		def s = "${d2(components[0])}:${d2(components[1])}:${d2(components[2])}${dfSep}${d2(components[3])}"
		s
	}
	
	private static final int ONE_HOUR_2997_FN = 17982
	private static final int ONE_MINUTE_2997_FN = 1798
	
	private int calculateDropedFramesToAdd2997(frameNumber) {
		def droppedFrames = 0
		int D = Math.floor(frameNumber / ONE_HOUR_2997_FN)
		int M = frameNumber % ONE_HOUR_2997_FN
		
		droppedFrames =  (18*D)

		if (M > 1) {
			int M1798 = Math.floor((M - 2) / ONE_MINUTE_2997_FN)
			droppedFrames += 2*(M1798)
		}
		if(M == 1) {
			// TODO raise error if not lenient
		}
		if(M == 0) {
			// TODO raise error if not lenient
		}

        return droppedFrames
	}
	private int calculateDropedFramesToAdd(frameNumber) {
		int droppedFrames = 0
		if (frameRate > 29.00 && frameRate < 30.00) {
			droppedFrames = calculateDropedFramesToAdd2997(frameNumber)
		}
		if (frameRate > 59.00 && frameRate < 60.00) {
			droppedFrames = calculateDropedFramesToAdd2997(frameNumber) * 2
		}
        return droppedFrames
	}
	
	private static String toCamelCase( String text, boolean capitalized = false ) {
        text = text.toLowerCase().replaceAll( "(_)([A-Za-z0-9])", { Object[] it -> it[2].toUpperCase() } )
        return capitalized ? text[0].toUpperCase() + text[1..-1] : text
    }
	
	def convert(value, from, to) {
        if (from==to)
            return value
        def val = this."${getMethodNameForConvert(from, to)}"(value)
        val
        
	}
	
	def getMethodNameForConvert(from, to) {
        def fromCamelCased = toCamelCase(from.toString(), false)
        def toCamelCased = toCamelCase(to.toString(), true)
        "${fromCamelCased}To${toCamelCased}"
	}
}