package gsat.timecode

import org.junit.Test;

class TimeCodeUtilsTests extends GroovyTestCase{
	
	def units = [TimeCodeUtils.Units.COMPONENTS, TimeCodeUtils.Units.FRAME_NUMBER, TimeCodeUtils.Units.MILLISECONDS ]
	
	def fr2997 = new TimeCodeUtils(frameRate: 29.97)
	
	def valid2997tcs = [
		[[0, 0, 0, 0], 0, 0, '00:00:00.00'],
		[[0, 0, 0, 1], 1, 33, '00:00:00.01'],
		[[0, 0, 0, 2], 2, 66, '00:00:00.02'],
		[[0, 0, 59, 28], 1798, 59993, '00:00:59.28'],
		[[0, 0, 59, 29], 1799, 60026, '00:00:59.29'],
		[[0, 1, 0, 2], 1800, 60060, '00:01:00.02'],
		[[0, 1, 0, 3], 1801, 60093, '00:01:00.03'],
		[[0, 4, 59, 28], 8990, 299966, '00:04:59.28'],
		[[0, 4, 59, 29], 8991, 300000, '00:04:59.29'],
		[[0, 5, 0, 2], 8992, 300033, '00:05:00.02'],
		[[0, 5, 0, 3], 8993, 300066, '00:05:00.03'],
		[[0, 9, 59, 28], 17980, 599933, '00:09:59.28'],
		[[0, 9, 59, 29], 17981, 599966, '00:09:59.29'],
		[[0, 10, 0, 0], 17982, 600000, '00:10:00.00'],
		[[0, 10, 0, 1], 17983, 600033, '00:10:00.01'],
		[[0, 10, 0, 2], 17984, 600066, '00:10:00.02'],
		[[0, 59, 59, 28], 107890, 3599933, '00:59:59.28'],
		[[0, 59, 59, 29], 107891, 3599966, '00:59:59.29'],
		[[1, 0, 0, 0], 107892, 3600000, '01:00:00.00'],
		[[1, 0, 0, 1], 107893, 3600033, '01:00:00.01'],
		[[1, 0, 0, 2], 107894, 3600066, '01:00:00.02'],
		[[23, 59, 59, 29], 2589407, 86399966, '23:59:59.29'],
	]
	
	@Test
    public void testRoundFrameRate() {
    	
    	def frs = [
    		[23.98, 24],
    		[25, 25],
    		[29.97, 30],
    		[30, 30],
    		[59.96, 60]
    	]
    	frs.each {
    		assertEquals(it[1], new TimeCodeUtils(frameRate: it[0]).roundFrameRate);
    	}
    }
    
    @Test
    public void testStringToFrameNumber2997() {
    	def tcu = fr2997
    	
    	valid2997tcs.each { tc ->
    		def s = tc[3]
    		def fn = tc[1]
    		assert tcu.stringToFrameNumber(s) == fn	
    	}
    }

    @Test
    public void testComponentsToFrameNumber2997() {
    	def tcu = fr2997
    	
    	valid2997tcs.each { tc ->
    		def c = tc[0]
    		def fn = tc[1]
    		assert tcu.componentsToFrameNumber(c) == fn	
    	}
    }

    @Test
    public void testFrameNumberToComponents2997() {
    	def tcu = fr2997
    	
    	valid2997tcs.each { tc ->
    		def c = tc[0]
    		def fn = tc[1]
    		assert tcu.frameNumberToComponents(fn) == c	
    	}
    }
    
    @Test
    public void testFrameNumberToString2997() {
    	def tcu = fr2997
    	
    	valid2997tcs.each { tc ->
    		def s = tc[3]
    		def fn = tc[1]
    		assert tcu.frameNumberToString(fn) == s	
    	}
    }
    
    @Test
    public void testMillisecondsToFrameNumber() {
    	def tcu = fr2997
    	
    	valid2997tcs.each { tc ->
    		def m = tc[2]
    		def fn = tc[1]
    		assert tcu.millisecondsToFrameNumber(m) == fn	
    	}
    }
    
    
    @Test
    public void testFrameNumberToMilliseconds2997() {
    	def tcu = fr2997
    	
    	valid2997tcs.each { tc ->
    		def m = tc[2]
    		def fn = tc[1]
    		assert tcu.frameNumberToMilliseconds(fn) == m	
    	}
    }

	@Test
    public void testMillisecondsToString2997() {
    	def tcu = fr2997
    	valid2997tcs.each { tc ->
    		def m = tc[2]
    		def s = tc[3]
    		assert tcu.millisecondsToString(m) == s	
    	}
    }

	@Test
    public void testMillisecondsToComponents2997() {
    	def tcu = fr2997
    	valid2997tcs.each { tc ->
    		def m = tc[2]
    		def c = tc[0]
    		assert tcu.millisecondsToComponents(m) == c	
    	}
    }

    @Test
    public void testStringToMilliseconds2997() {
    	def tcu = fr2997
    	
    	valid2997tcs.each { tc ->
    		def m = tc[2]
    		def s = tc[3]
    		assert tcu.stringToMilliseconds(s) == m	
    	}
    }

    @Test
    public void testComponentsToMilliseconds2997() {
    	def tcu = fr2997
    	
    	valid2997tcs.each { tc ->
    		def m = tc[2]
    		def c = tc[0]
    		assert tcu.componentsToMilliseconds(c) == m	
    	}
    }

    @Test
    public void testGetMethodNameForConvert() {
    	def tcu = fr2997
		assert tcu.getMethodNameForConvert(TimeCodeUtils.Units.MILLISECONDS, TimeCodeUtils.Units.COMPONENTS) == 'millisecondsToComponents'
		assert tcu.getMethodNameForConvert(TimeCodeUtils.Units.FRAME_NUMBER, TimeCodeUtils.Units.MILLISECONDS) == 'frameNumberToMilliseconds'
		assert tcu.getMethodNameForConvert(TimeCodeUtils.Units.COMPONENTS, TimeCodeUtils.Units.FRAME_NUMBER) == 'componentsToFrameNumber'
		assert tcu.getMethodNameForConvert(TimeCodeUtils.Units.STRING, TimeCodeUtils.Units.FRAME_NUMBER) == 'stringToFrameNumber'
		assert tcu.getMethodNameForConvert(TimeCodeUtils.Units.COMPONENTS, TimeCodeUtils.Units.STRING) == 'componentsToString'
    }

}